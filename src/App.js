import React, { useState, Fragment } from "react";

import "bootstrap/dist/css/bootstrap.css";

const App = () => {
  const [inputFields, setInputFields] = useState([
    { soal: "", jwb: [{ text: "", pertanyaan: [] }] },
  ]);

  const arrSoal = {
    soal: "",
    jwb: [{ text: "", pertanyaan: [] }],
  };

  const arrJwb = { text: "", pertanyaan: [] };

  const handleAddFields = (index, inJwb) => {
    const values = [...inputFields];

    if (inJwb !== "") {
      values[index].jwb[inJwb].pertanyaan.push(arrSoal);
    } else {
      values.push(arrSoal);
    }

    setInputFields(values);
  };

  const handleRemoveFields = (index) => {
    const values = [...inputFields];
    values.splice(index, 1);
    setInputFields(values);
  };

  const handleAddFieldsJawab = (index, i) => {
    const values = [...inputFields];
    values[index].jwb.push(arrJwb);
    setInputFields(values);
  };

  const handleRemoveFieldsJawab = (index, i) => {
    const values = [...inputFields];
    values[index].jwb.splice(i, 1);
    setInputFields(values);
  };

  const handleInputChange = (index, event) => {
    const values = [...inputFields];
    if (event.target.name === "soal") {
      values[index].soal = event.target.value;
    }

    setInputFields(values);
  };

  const handleInputChangeJawab = (index, i, event) => {
    const values = [...inputFields];
    const name = event.target.name;

    values[index].jwb[i][name] = event.target.value;

    setInputFields(values);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("inputFields", inputFields);
  };

  return (
    <>
      <h1>Dynamic Form</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-row row container">
          {inputFields.map((inputField, index) => (
            <Fragment key={`${inputField}~${index}`}>
              <div className="form-group col-sm-12">
                <label htmlFor="soal">Soal {index}</label>
                <input
                  type="text"
                  className="form-control"
                  id="soal"
                  name="soal"
                  value={inputField.soal}
                  onChange={(event) => handleInputChange(index, event)}
                />
              </div>
              <div className="form-group col-sm-12">
                <button
                  className="btn btn-link"
                  type="button"
                  onClick={() => handleRemoveFields(index, "")}
                >
                  hapus pertanyaan
                </button>
                <button
                  className="btn btn-link"
                  type="button"
                  onClick={() => handleAddFields(index, "")}
                >
                  tambah pertanyaan
                </button>
              </div>
              {inputField.jwb.map((item, i) => (
                <Fragment key={`${item}~${i}`}>
                  <div className="form-group col-sm-12">
                    <label htmlFor="text">Jawaban {i}</label>
                    <input
                      type="text"
                      className="form-control"
                      id="text"
                      name="text"
                      value={item.text}
                      onChange={(event) =>
                        handleInputChangeJawab(index, i, event)
                      }
                    />
                  </div>
                  <div className="form-group col-sm-12">
                    <button
                      className="btn btn-link"
                      type="button"
                      onClick={() => handleRemoveFieldsJawab(index, i)}
                    >
                      hapus jawaban
                    </button>
                    <button
                      className="btn btn-link"
                      type="button"
                      onClick={() => handleAddFieldsJawab(index, i)}
                    >
                      tambah jawaban
                    </button>
                  </div>
                  <div className="form-group col-sm-12">
                    <button
                      className="btn btn-link"
                      type="button"
                      onClick={() => handleRemoveFields(index, i)}
                    >
                      hapus pertanyaan
                    </button>
                    <button
                      className="btn btn-link"
                      type="button"
                      onClick={() => handleAddFields(index, i)}
                    >
                      tambah pertanyaan
                    </button>
                  </div>
                </Fragment>
              ))}
            </Fragment>
          ))}
          <div className="submit-button">
            <button
              className="btn btn-primary mr-2"
              type="submit"
              onSubmit={handleSubmit}
            >
              Save
            </button>
          </div>
        </div>

        <br />
        <pre>{JSON.stringify(inputFields, null, 2)}</pre>
      </form>
    </>
  );
};

export default App;
